import java.lang.Math;

public class Circle {
    int radius;
    Point center;

    public Circle(int r,Point p){
        radius=r;
        center=p;
    }

    public int area() {
        return (radius*radius);
    }
    public int perimeter(){
        return 2*radius;
    }
    public boolean intersect(Circle c){
        int r1=radius;
        Point p1=center;
        Double rangeOfTwoCenters=Math.sqrt(((p1.xCoord-c.center.xCoord)*(p1.xCoord-c.center.xCoord)) + ((p1.yCoord-c.center.yCoord)*(p1.yCoord-c.center.yCoord)));
        int rangeRadius=r1+c.radius;
        if (rangeRadius>rangeOfTwoCenters){
            return true;
        }
        else if (rangeRadius==rangeOfTwoCenters){
            return true;
        }
        else {
            return false;
        }


    }

}


