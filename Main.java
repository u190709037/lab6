public class Main {
    public static void main(String[] args) {
        Point p1=new Point(2,4);
        Rectangle rec1;
        rec1=new Rectangle(5,3,p1);

        Circle c1;
        Point p2=new Point(3,4);
        c1=new Circle(10,p2);

        Point p3=new Point(5,3);
        Circle c2=new Circle(6,p3);

        System.out.println(rec1.area());
        System.out.println(rec1.perimeter());
        //System.out.println(rec1.corners());
        System.out.println(c1.area());
        System.out.println(c1.perimeter());
        System.out.println(c1.intersect(c2));

    }
}
