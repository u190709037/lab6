public class Rectangle {
    int sideA;
    int sideB;
    Point topLeft;

    public Rectangle(int a,int b,Point p) {
        sideA=a;
        sideB=b;
        topLeft=p;
    }

    public int area() {
        return sideA*sideB ;
    }
    public int perimeter(){
        return 2*(sideA+sideB);
    }
    public String corners(){
        Point p1;
        p1=topLeft;
        Point p2;
        p2=p1;
        p2.xCoord+=sideA;
        Point p3;
        p3=p2;
        p3.yCoord-=sideB;
        Point p4;
        p4=p3;
        p4.xCoord-=sideA;

        return p1.xCoord+","+p1.yCoord+" "+ p2.xCoord+","+ p2.yCoord+" "+p3.xCoord+","+p3.yCoord+" "+p4.xCoord+","+p4.yCoord;

    }

}

